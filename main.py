import numpy as np
import pandas as pd
import copy

def read_data(scan_image, segmentation_image):
    df = pd.read_csv(scan_image, delimiter = ' ', header = None)
    img = np.array(df)
    df = pd.read_csv(segmentation_image, delimiter = ' ', header = None)
    seg = np.array(df)
    return img, seg

def get_interest_zone_boundaries(seg):
    min_list = []
    max_list = []
    for i in range(np.shape(seg)[0]):
        minimum = np.shape(seg)[1]
        maximum = 0
        for j in range(np.shape(seg)[1]):
            if seg[i][j] == 1:
                if j < minimum:
                    minimum = j
                if j > maximum:
                    maximum = j
        min_list.append(minimum)
        max_list.append(maximum)
    min_list = sorted(min_list)
    max_list = sorted(max_list, reverse = True)
    horizontal_min = min_list[0]
    horizontal_max = max_list[0]
    min_list = []
    max_list = []
    for i in range(np.shape(seg)[1]):
        minimum = np.shape(seg)[0]
        maximum = 0
        for j in range(np.shape(seg)[0]):
            if seg[j][i] == 1:
                if j < minimum:
                    minimum = j
                if j > maximum:
                    maximum = j
        min_list.append(minimum)
        max_list.append(maximum)
    min_list = sorted(min_list)
    max_list = sorted(max_list, reverse = True)
    vertical_min = min_list[0]
    vertical_max = max_list[0]

    horizontal_tolerance = round((5 / 100) * np.shape(seg)[1])
    vertical_tolerance = round((5 / 100) * np.shape(seg)[0])
    horizontal_min -= horizontal_tolerance
    if(horizontal_min < 0):
        horizontal_min = 0
    horizontal_max += horizontal_tolerance
    if(horizontal_max > np.shape(seg)[1]):
        horizontal_max = np.shape(seg)[1]
    vertical_min -= vertical_tolerance
    if(vertical_min < 0):
        vertical_min = 0
    vertical_max += vertical_tolerance
    if(vertical_max > np.shape(seg)[0]):
        vertical_max = np.shape(seg)[0]
    return vertical_min, vertical_max, horizontal_min, horizontal_max

def delete_weak_links(img, delta):
    img_copy = np.ones(shape=[np.shape(img)[0], np.shape(img)[1]])
    for i in range(np.shape(img)[0] - (delta - 1)):
        for j in range(np.shape(img)[1] - (delta - 1)):
            if np.all(np.array(img[i: i + delta, j: j + delta]) == 0):
                img_copy[i: i + delta, j: j + delta] = 0
    return img_copy

def delete_weak_links_2(img, delta):
    img_copy = np.zeros(shape=[np.shape(img)[0], np.shape(img)[1]])
    for i in range(np.shape(img)[0] - (delta - 1)):
        for j in range(np.shape(img)[1] - (delta - 1)):
            if np.all(np.array(img[i: i + delta, j: j + delta]) == 1):
                img_copy[i: i + delta, j: j + delta] = 1
    return img_copy

def get_segmentation_edges(seg, vertical_min, vertical_max, horizontal_min, horizontal_max):
    window = np.array([
        [1, 1, 1],
        [1, 1, 1],
        [1, 1, 1]
    ])
    seg_copy = copy.deepcopy(seg[vertical_min : vertical_max, horizontal_min : horizontal_max])
    seg_copy = delete_weak_links_2(seg_copy, 3)
    for i in range(vertical_min, vertical_max - 1):
        for j in range(horizontal_min, horizontal_max - 1):
            if np.array_equal(window, seg[i : i + 3, j : j + 3]):
                seg_copy[i - vertical_min + 1, j - horizontal_min + 1] = 0
    return seg_copy

def get_noisy_image_edges(img, vertical_min, vertical_max, horizontal_min, horizontal_max, d):
    img_copy = np.zeros(shape = [vertical_max - vertical_min - 2, horizontal_max - horizontal_min - 2])
    img_copy = np.pad(img_copy, pad_width = 1, constant_values = 1)
    for i in range(vertical_min, vertical_max - 2):
        for j in range(horizontal_min, horizontal_max - 2):
            window = np.array(img[i: i + 3, j: j + 3])
            maximum = window.max()
            minimum = window.min()
            delta = maximum - minimum
            if delta < d:
                img_copy[i - vertical_min + 1, j - horizontal_min + 1] = 1
    return img_copy

class Node:

    def __init__(self, center, left = None, down = None, right = None):
        self.center = center
        self.left = left
        self.down = down
        self.right = right

def build_tree(img, i, j):
    img[i, j] = 1
    if img[i, j - 1] == 0 and img[i + 1, j] == 0 and img[i, j + 1] == 0:
        return Node((i, j), left = build_tree(img, i, j - 1), down = build_tree(img, i + 1, j), right = build_tree(img, i, j + 1))
    elif img[i, j - 1] == 0 and img[i + 1, j] == 0:
        return Node((i, j), left = build_tree(img, i, j - 1), down = build_tree(img, i + 1, j))
    elif img[i, j - 1] == 0 and img[i, j + 1] == 0:
        return Node((i, j), left = build_tree(img, i, j - 1), right = build_tree(img, i, j + 1))
    elif img[i + 1, j] == 0 and img[i, j + 1] == 0:
        return Node((i, j), down = build_tree(img, i + 1, j), right = build_tree(img, i, j + 1))
    elif img[i, j - 1] == 0:
        return Node((i, j), left = build_tree(img, i, j - 1))
    elif img[i + 1, j] == 0:
        return Node((i, j), down = build_tree(img, i + 1, j))
    elif img[i, j + 1] == 0:
        return Node((i, j), right = build_tree(img, i, j + 1))
    else:
       return Node((i, j))

def read_tree(parent, s):

    if parent.left != None and parent.down != None and parent.right != None:
        s.add(parent.center)
        s = s.union(read_tree(parent.left, s))
        s = s.union(read_tree(parent.down, s))
        s = s.union(read_tree(parent.right, s))
        return s
    elif parent.left != None and parent.down != None:
        s.add(parent.center)
        s = s.union(read_tree(parent.left, s))
        s = s.union(read_tree(parent.down, s))
        return s
    elif parent.left != None and parent.right != None:
        s.add(parent.center)
        s = s.union(read_tree(parent.left, s))
        s = s.union(read_tree(parent.right, s))
        return s
    elif parent.down != None and parent.right != None:
        s.add(parent.center)
        s = s.union(read_tree(parent.down, s))
        s = s.union(read_tree(parent.right, s))
        return s
    elif parent.left != None:
        s.add(parent.center)
        s = s.union(read_tree(parent.left, s))
        return s
    elif parent.down != None:
        s.add(parent.center)
        s = s.union(read_tree(parent.down, s))
        return s
    elif parent.right != None:
        s.add(parent.center)
        s = s.union(read_tree(parent.right, s))
        return s
    else:
        s.add(parent.center)
        return s

def get_denoised_image(img_copy, delta):
    d = {}
    for i in range(np.shape(img_copy)[0]):
        for j in range(np.shape(img_copy)[1]):
            if img_copy[i, j] == 0:
                parent = build_tree(img_copy, i, j)
                s = set({})
                s = read_tree(parent, s)
                l = sorted(list(s))
                img_copy[l[0]] = 0
                d[l[0]] = l

    img_denoised = np.ones(shape = [np.shape(img_copy)[0], np.shape(img_copy)[1]])
    for i in range(np.shape(img_copy)[0]):
        for j in range(np.shape(img_copy)[1]):
            if img_copy[i, j] == 0 and len(d[(i, j)]) > delta:
                for k, l in d[(i, j)]:
                    img_denoised[k, l] = 0
    return img_denoised

def pad_down(img, delta):
    for i in range(1, np.shape(img)[0] - 1):
        ones_group = []
        for j in range(1, np.shape(img)[1] - 1):
            if len(ones_group) > delta - 1:
                ones_group.clear()
            if img[i, j] == 1:
                if np.all(img[i - 1, j - 1: j + 2] == 0) and img[i, j - 1] == 0 and img[i, j + 1] == 0:
                    img[i, j] = 0
                elif np.all(img[i - 1: i + 1, j - 1] == 0) and img[i - 1, j] == 0:
                    ones_group.clear()
                    ones_group.append(-1)
                elif np.all(img[i - 1: i + 1, j + 1] == 0) and img[i - 1, j] == 0 and len(ones_group) != 0:
                    if ones_group[0] == -1:
                        img[i, j - len(ones_group): j + 1] = 0
                elif img[i - 1, j] == 0:
                    ones_group.append(1)
                else:
                    ones_group.clear()
            else:
                ones_group.clear()
    return img

def pad_up(img, delta):
    for i in range(np.shape(img)[0] - 2, 1, -1):
        ones_group = []
        for j in range(1, np.shape(img)[1] - 1):
            if len(ones_group) > delta - 1:
                ones_group.clear()
            if img[i, j] == 1:
                if np.all(img[i + 1, j - 1: j + 2] == 0) and img[i, j - 1] == 0 and img[i, j + 1] == 0:
                    img[i, j] = 0
                elif np.all(img[i: i + 2, j - 1] == 0) and img[i + 1, j] == 0:
                    ones_group.clear()
                    ones_group.append(-1)
                elif np.all(img[i: i + 2, j + 1] == 0) and img[i + 1, j] == 0 and len(ones_group) != 0:
                    if ones_group[0] == -1:
                        img[i, j - len(ones_group): j + 1] = 0
                elif img[i + 1, j] == 0:
                    ones_group.append(1)
                else:
                    ones_group.clear()
            else:
                ones_group.clear()
    return img

def pad_left(img, delta):
    for i in range(np.shape(img)[1] - 2, 1, -1):
        ones_group = []
        for j in range(1, np.shape(img)[0] - 1):
            if len(ones_group) > delta - 1:
                ones_group.clear()
            if img[j, i] == 1:
                if np.all(img[j - 1: j + 2, i + 1] == 0) and img[j - 1, i] == 0 and img[j + 1, i] == 0:
                    img[j, i] = 0
                elif np.all(img[j - 1: j + 1, i + 1] == 0) and img[j - 1, i] == 0:
                    ones_group.clear()
                    ones_group.append(-1)
                elif np.all(img[j: j + 2, i + 1] == 0) and img[j + 1, i] == 0 and len(ones_group) != 0:
                    if ones_group[0] == -1:
                        img[j - len(ones_group): j + 1, i] = 0
                elif img[j, i + 1] == 0:
                    ones_group.append(1)
                else:
                    ones_group.clear()
            else:
                ones_group.clear()
    return img

def pad_right(img, delta):
    for i in range(1, np.shape(img)[1] - 1):
        ones_group = []
        for j in range(1, np.shape(img)[0] - 1):
            if len(ones_group) > delta - 1:
                ones_group.clear()
            if img[j, i] == 1:
                if np.all(img[j - 1: j + 2, i - 1] == 0) and img[j - 1, i] == 0 and img[j + 1, i] == 0:
                    img[j, i] = 0
                elif np.all(img[j - 1: j + 1, i - 1] == 0) and img[j - 1, i] == 0:
                    ones_group.clear()
                    ones_group.append(-1)
                elif np.all(img[j: j + 2, i - 1] == 0) and img[j + 1, i] == 0 and len(ones_group) != 0:
                    if ones_group[0] == -1:
                        img[j - len(ones_group): j + 1, i] = 0
                elif img[j, i - 1] == 0:
                    ones_group.append(1)
                else:
                    ones_group.clear()
            else:
                ones_group.clear()
    return img

def approximate_corners(img_denoised):
    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if img_denoised[i, j] == 0 and img_denoised[i - 1, j] == 0 and img_denoised[i, j + 1] == 0 and img_denoised[i - 1, j + 1] == 1:
                start = j
                end = j + 1
                for k in range(j + 1, np.shape(img_denoised)[1] - 1):
                    if img_denoised[i, k] == 0 and img_denoised[i - 1, k] == 1 and img_denoised[i, k + 1] == 0:
                        continue
                    elif img_denoised[i, k] == 0 and img_denoised[i - 1, k] == 1 and img_denoised[i, k + 1] == 1 and \
                            img_denoised[i - 1, k + 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    width = end - start
                end = i
                start = i - 1
                for k in range(i - 1, 1, -1):
                    if img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k - 1, j] == 0:
                        continue
                    elif img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k - 1, j] == 1 and \
                            img_denoised[k - 1, j + 1] == 1:
                        start = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    height = end - start
                delta = height / width
                for k in range(width):
                    img_denoised[i - round(height - delta): i, j + 1 + k] = np.zeros(shape=[round(height - delta)])
                    height -= delta

    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if img_denoised[i, j] == 0 and img_denoised[i - 1, j] == 0 and img_denoised[i, j - 1] == 0 and img_denoised[i - 1, j - 1] == 1:
                start = j - 1
                end = j
                for k in range(j - 1, 1, -1):
                    if img_denoised[i, k] == 0 and img_denoised[i - 1, k] == 1 and img_denoised[i, k - 1] == 0:
                        continue
                    elif img_denoised[i, k] == 0 and img_denoised[i - 1, k] == 1 and img_denoised[i, k - 1] == 1 and \
                            img_denoised[i - 1, k - 1] == 1:
                        start = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    width = end - start
                end = i
                start = i - 1
                for k in range(i - 1, 1, -1):
                    if img_denoised[k, j] == 0 and img_denoised[k, j - 1] == 1 and img_denoised[k - 1, j] == 0:
                        continue
                    elif img_denoised[k, j] == 0 and img_denoised[k, j - 1] == 1 and img_denoised[k - 1, j] == 1 and \
                            img_denoised[k - 1, j - 1] == 1:
                        start = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    height = end - start
                delta = height / width
                for k in range(width):
                    img_denoised[i - round(height - delta): i, j - 1 - k] = np.zeros(shape=[round(height - delta)])
                    height -= delta

    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if img_denoised[i, j] == 0 and img_denoised[i + 1, j] == 0 and img_denoised[i, j + 1] == 0 and img_denoised[i + 1, j + 1] == 1:
                start = j
                end = j + 1
                for k in range(j + 1, np.shape(img_denoised)[1] - 1):
                    if img_denoised[i, k] == 0 and img_denoised[i + 1, k] == 1 and img_denoised[i, k + 1] == 0:
                        continue
                    elif img_denoised[i, k] == 0 and img_denoised[i + 1, k] == 1 and img_denoised[i, k + 1] == 1 and \
                            img_denoised[i + 1, k + 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    width = end - start
                end = i + 1
                start = i
                for k in range(i + 1, np.shape(img_denoised)[0] - 1):
                    if img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k + 1, j] == 0:
                        continue
                    elif img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k + 1, j] == 1 and \
                            img_denoised[k + 1, j + 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    height = end - start
                delta = height / width
                for k in range(width):
                    img_denoised[i + 1: i + round(height - delta) + 1, j + 1 + k] = np.zeros(
                        shape=[round(height - delta)])
                    height -= delta

    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if img_denoised[i, j] == 0 and img_denoised[i + 1, j] == 0 and img_denoised[i, j - 1] == 0 and img_denoised[i + 1, j - 1] == 1:
                start = j - 1
                end = j
                for k in range(j - 1, 1, - 1):
                    if img_denoised[i, k] == 0 and img_denoised[i + 1, k] == 1 and img_denoised[i, k - 1] == 0:
                        continue
                    elif img_denoised[i, k] == 0 and img_denoised[i + 1, k] == 1 and img_denoised[i, k - 1] == 1 and \
                            img_denoised[i - 1, k - 1] == 1:
                        start = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    width = end - start
                end = i + 1
                start = i
                for k in range(i + 1, np.shape(img_denoised)[0] - 1):
                    if img_denoised[k, j] == 0 and img_denoised[k, j - 1] == 1 and img_denoised[k + 1, j] == 0:
                        continue
                    elif img_denoised[k, j] == 0 and img_denoised[k, j - 1] == 1 and img_denoised[k + 1, j] == 1 and \
                            img_denoised[k + 1, j - 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    height = end - start
                delta = height / width
                for k in range(width):
                    img_denoised[i + 1: i + round(height - delta) + 1, j - 1 - k] = np.zeros(
                        shape=[round(height - delta)])
                    height -= delta

    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if img_denoised[i, j] == 0 and img_denoised[i - 1, j] == 0 and img_denoised[i, j + 1] == 0 and img_denoised[i - 1, j + 1] == 1:
                start = j
                end = j + 1
                for k in range(j + 1, np.shape(img_denoised)[1] - 1):
                    if img_denoised[i, k] == 0 and img_denoised[i - 1, k] == 1 and img_denoised[i, k + 1] == 0:
                        continue
                    elif img_denoised[i, k] == 0 and img_denoised[i - 1, k] == 0 and img_denoised[i, k - 1] == 0 and \
                            img_denoised[i - 1, k - 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    width = end - start
                end = i
                start = i - 1
                for k in range(i - 1, 1, -1):
                    if img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k - 1, j] == 0:
                        continue
                    elif img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k - 1, j] == 1 and \
                            img_denoised[k - 1, j + 1] == 1:
                        start = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    left_height = end - start
                delta = left_height / round(width / 2)
                for k in range(round(width / 2)):
                    img_denoised[i - round(left_height - delta): i, j + 1 + k] = np.zeros(
                        shape=[round(left_height - delta)])
                    left_height -= delta
                end = i
                start = i - 1
                for k in range(i - 1, 1, -1):
                    if img_denoised[k, j + width] == 0 and img_denoised[k, j + width - 1] == 1 and img_denoised[k - 1, j + width] == 0:
                        continue
                    elif img_denoised[k, j + width] == 0 and img_denoised[k, j + width - 1] == 1 and img_denoised[
                        k - 1, j + width] == 1 and img_denoised[k - 1, j + width - 1] == 1:
                        start = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    right_height = end - start
                delta = right_height / round(width / 2)
                for k in range(round(width / 2)):
                    img_denoised[i - round(right_height - delta): i, j + width - 1 - k] = np.zeros(
                        shape=[round(right_height - delta)])
                    right_height -= delta

    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if img_denoised[i, j] == 0 and img_denoised[i + 1, j] == 0 and img_denoised[i, j + 1] == 0 and img_denoised[i + 1, j + 1] == 1:
                start = j
                end = j + 1
                for k in range(j + 1, np.shape(img_denoised)[1] - 1):
                    if img_denoised[i, k] == 0 and img_denoised[i + 1, k] == 1 and img_denoised[i, k + 1] == 0:
                        continue
                    elif img_denoised[i, k] == 0 and img_denoised[i + 1, k] == 0 and img_denoised[i, k - 1] == 0 and \
                            img_denoised[i + 1, k - 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    width = end - start
                end = i + 1
                start = i
                for k in range(i + 1, np.shape(img_denoised)[0] - 1):
                    if img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k + 1, j] == 0:
                        continue
                    elif img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k + 1, j] == 1 and \
                            img_denoised[k + 1, j + 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    left_height = end - start
                delta = left_height / round(width / 2)
                for k in range(round(width / 2)):
                    img_denoised[i + 1: i + round(left_height - delta) + 1, j + 1 + k] = np.zeros(
                        shape=[round(left_height - delta)])
                    left_height -= delta
                end = i + 1
                start = i
                for k in range(i + 1, np.shape(img_denoised)[0] - 1):
                    if img_denoised[k, j + width] == 0 and img_denoised[k, j + width - 1] == 1 and img_denoised[k + 1, j + width] == 0:
                        continue
                    elif img_denoised[k, j + width] == 0 and img_denoised[k, j + width - 1] == 1 and img_denoised[
                        k + 1, j + width] == 1 and img_denoised[k + 1, j + width - 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    right_height = end - start
                delta = right_height / round(width / 2)
                for k in range(round(width / 2)):
                    img_denoised[i + 1: i + round(right_height - delta) + 1, j + width - 1 - k] = np.zeros(
                        shape=[round(right_height - delta)])
                    right_height -= delta

    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if img_denoised[i, j] == 0 and img_denoised[i + 1, j] == 0 and img_denoised[i, j + 1] == 0 and img_denoised[i + 1, j + 1] == 1:
                start = i
                end = i + 1
                for k in range(i + 1, np.shape(img_denoised)[0] - 1):
                    if img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 1 and img_denoised[k + 1, j] == 0:
                        continue
                    elif img_denoised[k, j] == 0 and img_denoised[k, j + 1] == 0 and img_denoised[k - 1, j] == 0 and \
                            img_denoised[k - 1, j + 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    height = end - start
                end = j + 1
                start = j
                for k in range(j + 1, np.shape(img_denoised)[1] - 1):
                    if img_denoised[i, k] == 0 and img_denoised[i + 1, k] == 1 and img_denoised[i, k + 1] == 0:
                        continue
                    elif img_denoised[i, k] == 0 and img_denoised[i, k + 1] == 1 and img_denoised[i + 1, k] == 1 and \
                            img_denoised[i + 1, k + 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    upper_width = end - start
                delta = upper_width / round(height / 2)
                for k in range(round(height / 2)):
                    img_denoised[i + 1 + k, j + 1: j + round(upper_width - delta) + 1] = np.zeros(
                        shape=[round(upper_width - delta)])
                    upper_width -= delta
                end = j + 1
                start = j
                for k in range(j + 1, np.shape(img_denoised)[1] - 1):
                    if img_denoised[i + height, k] == 0 and img_denoised[i + height - 1, k] == 1 and img_denoised[i + height, k + 1] == 0:
                        continue
                    elif img_denoised[i + height, k] == 0 and img_denoised[i + height - 1, k] == 1 and img_denoised[
                        i + height, k + 1] == 1 and img_denoised[i + height - 1, k + 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    lower_width = end - start
                delta = lower_width / round(height / 2)
                for k in range(round(height / 2)):
                    img_denoised[i + height - 1 - k, j + 1: j + round(lower_width - delta) + 1] = np.zeros(
                        shape=[round(lower_width - delta)])
                    lower_width -= delta

    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if img_denoised[i, j] == 0 and img_denoised[i + 1, j] == 0 and img_denoised[i, j - 1] == 0 and img_denoised[i + 1, j - 1] == 1:
                start = i
                end = i + 1
                for k in range(i + 1, np.shape(img_denoised)[0] - 1):
                    if img_denoised[k, j] == 0 and img_denoised[k, j - 1] == 1 and img_denoised[k + 1, j] == 0:
                        continue
                    elif img_denoised[k, j] == 0 and img_denoised[k, j - 1] == 0 and img_denoised[k - 1, j] == 0 and \
                            img_denoised[k - 1, j - 1] == 1:
                        end = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    height = end - start
                end = j
                start = j - 1
                for k in range(j - 1, 1, -1):
                    if img_denoised[i, k] == 0 and img_denoised[i + 1, k] == 1 and img_denoised[i, k - 1] == 0:
                        continue
                    elif img_denoised[i, k] == 0 and img_denoised[i, k - 1] == 1 and img_denoised[i + 1, k] == 1 and \
                            img_denoised[i + 1, k - 1] == 1:
                        start = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    upper_width = end - start
                delta = upper_width / round(height / 2)
                for k in range(round(height / 2)):
                    img_denoised[i + 1 + k, j - round(upper_width - delta): j] = np.zeros(
                        shape=[round(upper_width - delta)])
                    upper_width -= delta
                end = j
                start = j - 1
                for k in range(j - 1, 1, -1):
                    if img_denoised[i + height, k] == 0 and img_denoised[i + height - 1, k] == 1 and img_denoised[i + height, k - 1] == 0:
                        continue
                    elif img_denoised[i + height, k] == 0 and img_denoised[i + height - 1, k] == 1 and img_denoised[
                        i + height, k - 1] == 1 and img_denoised[i + height - 1, k - 1] == 1:
                        start = k
                        break
                    else:
                        end = -1
                        break
                if end == -1:
                    break
                else:
                    lower_width = end - start
                delta = lower_width / round(height / 2)
                for k in range(round(height / 2)):
                    img_denoised[i + height - 1 - k, j - round(lower_width - delta): j] = np.zeros(
                        shape=[round(lower_width - delta)])
                    lower_width -= delta
    img_denoised = delete_weak_links(img_denoised, 3)
    return img_denoised

def get_image_contour(img_denoised):
    contour = copy.deepcopy(img_denoised)
    for i in range(1, np.shape(img_denoised)[0] - 1):
        for j in range(1, np.shape(img_denoised)[1] - 1):
            if np.all(np.array(img_denoised[i - 1 : i + 2, j - 1 : j + 2]) == 0):
                contour[i, j] = 1
    return contour

def get_inside_sections(seg_copy):
    inside = copy.deepcopy(seg_copy)
    for i in range(np.shape(inside)[0]):
        for j in range(np.shape(inside)[1]):
            if inside[i, j] != img_denoised[i, j]:
                inside[i, j] = 0
    return inside

def get_outside_sections(seg_copy):
    outside = copy.deepcopy(seg_copy)
    for i in range(np.shape(outside)[0]):
        for j in range(np.shape(outside)[1]):
            if outside[i, j] == inside[i, j]:
                outside[i, j] = 0
    return outside

def eliminate_single_pixels(inside, outside):
    for i in range(1, np.shape(inside)[0] - 1):
        for j in range(1, np.shape(inside)[1] - 1):
            if inside[i, j] == 1 and inside[i - 1, j] == 0 and inside[i, j + 1] == 0 and inside[i + 1, j] == 0 and inside[i, j - 1] == 0:
                inside[i, j] = 0
                outside[i, j] = 1
            if outside[i, j] == 1 and outside[i - 1, j] == 0 and outside[i, j + 1] == 0 and outside[i + 1, j] == 0 and outside[i, j - 1] == 0:
                outside[i, j] = 0
                inside[i, j] = 1
    return inside, outside

class Node_1_leaf:

    def __init__(self, center, left = None, down = None, right = None, left_down = None, right_down = None, left_up = None, right_up = None, up = None):
        self.center = center
        self.left = left
        self.down = down
        self.right = right
        self.left_down = left_down
        self.right_down = right_down
        self.left_up = left_up
        self.right_up = right_up
        self.up = up

def build_tree_1_leaf(img, i, j):
    img[i, j] = 0

    if img[i, j + 1] == 1 and img[i + 1, j] == 1:
        return Node_1_leaf((i, j), down = build_tree_1_leaf(img, i + 1, j), right = build_tree_1_leaf(img, i, j + 1))
    elif img[i, j + 1] == 1 and img[i + 1, j - 1] == 1:
        return Node_1_leaf((i, j), left_down = build_tree_1_leaf(img, i + 1, j - 1), right = build_tree_1_leaf(img, i, j + 1))
    elif img[i - 1, j] == 1 and img[i - 1, j + 1] == 1:
        return Node_1_leaf((i, j), up = build_tree_1_leaf(img, i - 1, j))
    elif img[i, j + 1] == 1 and img[i - 1, j + 1] == 1:
        return Node_1_leaf((i, j), right = build_tree_1_leaf(img, i, j + 1))
    elif img[i, j + 1] == 1 and img[i + 1, j + 1] == 1:
        return Node_1_leaf((i, j), right = build_tree_1_leaf(img, i, j + 1))
    elif img[i + 1, j] == 1 and img[i + 1, j + 1] == 1:
        return Node_1_leaf((i, j), down = build_tree_1_leaf(img, i + 1, j))
    elif img[i + 1, j] == 1 and img[i + 1, j - 1] == 1:
        return Node_1_leaf((i, j), down = build_tree_1_leaf(img, i + 1, j))
    elif img[i, j - 1] == 1 and img[i + 1, j - 1] == 1:
        return Node_1_leaf((i, j), left = build_tree_1_leaf(img, i, j - 1))
    elif img[i, j - 1] == 1 and img[i - 1, j - 1] == 1:
        return Node_1_leaf((i, j), left = build_tree_1_leaf(img, i, j - 1))
    elif img[i - 1, j] == 1 and img[i - 1, j - 1] == 1:
        return Node_1_leaf((i, j), up = build_tree_1_leaf(img, i - 1, j))
    elif img[i - 1, j] == 1:
        return Node_1_leaf((i, j), up = build_tree_1_leaf(img, i - 1, j))
    elif img[i, j + 1] == 1:
        return Node_1_leaf((i, j), right = build_tree_1_leaf(img, i, j + 1))
    elif img[i + 1, j + 1] == 1:
        return Node_1_leaf((i, j), right_down = build_tree_1_leaf(img, i + 1, j + 1))
    elif img[i + 1, j] == 1:
        return Node_1_leaf((i, j), down = build_tree_1_leaf(img, i + 1, j))
    elif img[i + 1, j - 1] == 1:
        return Node_1_leaf((i, j), left_down = build_tree_1_leaf(img, i + 1, j - 1))
    elif img[i, j - 1] == 1:
        return Node_1_leaf((i, j), left = build_tree_1_leaf(img, i, j - 1))
    elif img[i - 1, j - 1] == 1:
        return Node_1_leaf((i, j), left_up = build_tree_1_leaf(img, i - 1, j - 1))
    elif img[i - 1, j + 1] == 1:
        return Node_1_leaf((i, j), right_up = build_tree_1_leaf(img, i - 1, j + 1))
    else:
        return Node_1_leaf((i, j))

def read_tree_1_leaf(parent, s):

    if parent.right != None and parent.down != None:
        s = s.union(read_tree_1_leaf(parent.right, s))
        s = s.union(read_tree_1_leaf(parent.down, s))
        return s
    elif parent.right != None and parent.left_down != None:
        s = s.union(read_tree_1_leaf(parent.right, s))
        s = s.union(read_tree_1_leaf(parent.left_down, s))
        return s
    elif parent.left != None:
        s = s.union(read_tree_1_leaf(parent.left, s))
        return s
    elif parent.left_down != None:
        s = s.union(read_tree_1_leaf(parent.left_down, s))
        return s
    elif parent.down != None:
        s = s.union(read_tree_1_leaf(parent.down, s))
        return s
    elif parent.right_down != None:
        s = s.union(read_tree_1_leaf(parent.right_down, s))
        return s
    elif parent.right != None:
        s = s.union(read_tree_1_leaf(parent.right, s))
        return s
    elif parent.right_up != None:
        s = s.union(read_tree_1_leaf(parent.right_up, s))
        return s
    elif parent.up != None:
        s = s.union(read_tree_1_leaf(parent.up, s))
        return s
    elif parent.left_up != None:
        s = s.union(read_tree_1_leaf(parent.left_up, s))
        return s
    else:
        s.add(parent.center)
        return s

class Node_1_leaf_2:

    def __init__(self, center, index, up = None, right = None, down = None, left = None):
        self.center = center
        self.index = index
        self.up = up
        self.right = right
        self.down = down
        self.left = left

def build_tree_1_leaf_2(img, i, j, m, n, max_iter, counter):
    img[i, j] = 1
    counter += 1

    if (i == m and j == n) or max_iter == counter:
        return Node_1_leaf_2((i, j), counter)

    if img[i - 1, j] == 0:
        return Node_1_leaf_2((i, j), counter, up = build_tree_1_leaf_2(img, i - 1, j, m, n, max_iter, counter))
    elif img[i, j + 1] == 0:
        return Node_1_leaf_2((i, j), counter, right = build_tree_1_leaf_2(img, i, j + 1, m, n, max_iter, counter))
    elif img[i + 1, j] == 0:
        return Node_1_leaf_2((i, j), counter, down = build_tree_1_leaf_2(img, i + 1, j, m, n, max_iter, counter))
    elif img[i, j - 1] == 0:
        return Node_1_leaf_2((i, j), counter, left = build_tree_1_leaf_2(img, i, j - 1, m, n, max_iter, counter))
    else:
        return Node_1_leaf_2((i, j), counter)

def build_tree_1_leaf_3(img, i, j, m, n, max_iter, counter):
    img[i, j] = 0
    counter += 1

    if (i == m and j == n) or max_iter == counter:
        return Node_1_leaf_2((i, j), counter)

    if img[i - 1, j] == 1:
        return Node_1_leaf_2((i, j), counter, up = build_tree_1_leaf_3(img, i - 1, j, m, n, max_iter, counter))
    elif img[i, j + 1] == 1:
        return Node_1_leaf_2((i, j), counter, right = build_tree_1_leaf_3(img, i, j + 1, m, n, max_iter, counter))
    elif img[i + 1, j] == 1:
        return Node_1_leaf_2((i, j), counter, down = build_tree_1_leaf_3(img, i + 1, j, m, n, max_iter, counter))
    elif img[i, j - 1] == 1:
        return Node_1_leaf_2((i, j), counter, left = build_tree_1_leaf_3(img, i, j - 1, m, n, max_iter, counter))
    else:
        return Node_1_leaf_2((i, j), counter)

def read_tree_1_leaf_2(parent, d):

    if parent.left != None:
        d[parent.index] = parent.center
        return read_tree_1_leaf_2(parent.left, d)
    elif parent.down != None:
        d[parent.index] = parent.center
        return read_tree_1_leaf_2(parent.down, d)
    elif parent.right != None:
        d[parent.index] = parent.center
        return read_tree_1_leaf_2(parent.right, d)
    elif parent.up != None:
        d[parent.index] = parent.center
        return read_tree_1_leaf_2(parent.up, d)
    else:
        d[parent.index] = parent.center
        return d

def get_start_end_points_of_outside_sections(outside):
    d = {}
    for i in range(np.shape(outside)[0]):
        for j in range(np.shape(outside)[1]):
            if outside[i, j] == 1:
                parent = build_tree_1_leaf(outside, i, j)
                s = set({})
                s = read_tree_1_leaf(parent, s)
                l = list(s)
                l = sorted(l)
                if len(l) == 1:
                    d[(i, j)] = l[0]
                else:
                    d[l[0]] = l[1]
    return d

def correct_outside_sections(contour, d):
    contour_copy = np.ones(shape = [np.shape(contour)[0], np.shape(contour)[1]])
    for start, finish in list(d.items()):
        contour[start[0], start[1]] = 1
        l = []
        if contour[start[0] - 1, start[1]] == 0:
            l.append((start[0] - 1, start[1]))
        if contour[start[0], start[1] + 1] == 0:
            l.append((start[0], start[1] + 1))
        if contour[start[0] + 1, start[1]] == 0:
            l.append((start[0] + 1, start[1]))
        if contour[start[0], start[1] - 1] == 0:
            l.append((start[0], start[1] - 1))

        max_iter1 = 20
        max_iter2 = 20
        while(1):
            parent1 = build_tree_1_leaf_2(contour, l[0][0], l[0][1], finish[0], finish[1], max_iter1, 0)
            d1 = {}
            d1 = read_tree_1_leaf_2(parent1, d1)
            l1 = sorted(list(d1.items()))
            if l1[-1][0] == max_iter1 and (l1[-1][1][0] != finish[0] or l1[-1][1][1] != finish[1]):
                max_iter1 = max_iter1 * 2
                for i, j in list(d1.values()):
                    contour[i, j] = 0
            else:
                l.clear()
                l.append(start)
                l.extend(list(d1.values()))
                for i, j in l:
                    contour[i, j] = 0
                    contour_copy[i, j] = 0
                break
            parent2 = build_tree_1_leaf_2(contour, l[1][0], l[1][1], finish[0], finish[1], max_iter2, 0)
            d2 = {}
            d2 = read_tree_1_leaf_2(parent2, d2)
            l2 = sorted(list(d2.items()))
            if l2[-1][0] == max_iter2 and (l2[-1][1][0] != finish[0] or l2[-1][1][1] != finish[1]):
                max_iter2 = max_iter2 * 2
                for i, j in list(d2.values()):
                    contour[i, j] = 0
            else:
                l.clear()
                l.append(start)
                l.extend(list(d2.values()))
                for i, j in l:
                    contour[i, j] = 0
                    contour_copy[i, j] = 0
                break
    return contour_copy, contour

def correct_inside_sections(inside, d, contour, contour_copy, max):
    inside_copy = copy.deepcopy(inside)
    d2 = {}
    for i in range(np.shape(inside_copy)[0]):
        for j in range(np.shape(inside_copy)[1]):
            if inside_copy[i, j] == 1:
                parent = build_tree_1_leaf(inside_copy, i, j)
                s = set({})
                s = read_tree_1_leaf(parent, s)
                l = list(s)
                l = sorted(l)
                if len(l) == 1:
                    d2[(i, j)] = l[0]
                else:
                    d2[l[0]] = l[1]

    l = list(d.keys())
    l.extend(list(d.values()))
    l2 = list(d2.keys())
    l2.extend(list(d2.values()))
    d3 = {}
    for i in range(len(l2)):
        dt = np.dtype('int, int')
        if np.any(np.array(l, dtype = dt) == np.array([(l2[i][0] - 1, l2[i][1])], dtype = dt)):
            d3[l2[i]] = (l2[i][0] - 1, l2[i][1])
        elif np.any(np.array(l, dtype = dt) == np.array([(l2[i][0], l2[i][1] + 1)], dtype = dt)):
            d3[l2[i]] = (l2[i][0], l2[i][1] + 1)
        elif np.any(np.array(l, dtype = dt) == np.array([(l2[i][0] + 1, l2[i][1])], dtype = dt)):
            d3[l2[i]] = (l2[i][0] + 1, l2[i][1])
        elif np.any(np.array(l, dtype = dt) == np.array([(l2[i][0], l2[i][1] - 1)], dtype = dt)):
            d3[l2[i]] = (l2[i][0], l2[i][1] - 1)

    d = {}
    for i, j in list(d2.items()):
        d[d3[i]] = d3[j]
    d4 = {}
    for i, j in list(d3.items()):
        d4[j] = i

    for start, finish in list(d.items()):
        contour[start[0], start[1]] = 1
        l = []
        if contour[start[0] - 1, start[1]] == 0:
            l.append((start[0] - 1, start[1]))
        if contour[start[0], start[1] + 1] == 0:
            l.append((start[0], start[1] + 1))
        if contour[start[0] + 1, start[1]] == 0:
            l.append((start[0] + 1, start[1]))
        if contour[start[0], start[1] - 1] == 0:
            l.append((start[0], start[1] - 1))

        max_iter1 = 20
        max_iter2 = 20
        while(1):
            parent1 = build_tree_1_leaf_2(contour, l[0][0], l[0][1], finish[0], finish[1], max_iter1, 0)
            d1 = {}
            d1 = read_tree_1_leaf_2(parent1, d1)
            l1 = sorted(list(d1.items()))
            if l1[-1][0] == max_iter1 and (l1[-1][1][0] != finish[0] or l1[-1][1][1] != finish[1]):
                max_iter1 = max_iter1 * 2
                for i, j in list(d1.values()):
                    contour[i, j] = 0
            else:
                l.clear()
                l.append(start)
                l.extend(list(d1.values()))
                for i, j in l:
                    contour[i, j] = 0
                    contour_copy[i, j] = 0
                break
            parent2 = build_tree_1_leaf_2(contour, l[1][0], l[1][1], finish[0], finish[1], max_iter2, 0)
            d2 = {}
            d2 = read_tree_1_leaf_2(parent2, d2)
            l2 = sorted(list(d2.items()))
            if l2[-1][0] == max_iter2 and (l2[-1][1][0] != finish[0] or l2[-1][1][1] != finish[1]):
                max_iter2 = max_iter2 * 2
                for i, j in list(d2.values()):
                    contour[i, j] = 0
            else:
                l.clear()
                l.append(start)
                l.extend(list(d2.values()))
                for i, j in l:
                    contour[i, j] = 0
                    contour_copy[i, j] = 0
                break
            if max_iter1 > max and max_iter2 > max:
                contour[start[0], start[1]] = 0
                parent1 = build_tree_1_leaf_3(inside, d4[start][0], d4[start][1], d4[finish][0], d4[finish][1], max_iter1, 0)
                d1 = {}
                d1 = read_tree_1_leaf_2(parent1, d1)
                l1 = sorted(list(d1.items()))
                l.clear()
                l.extend(list(d1.values()))
                for i, j in l:
                    contour_copy[i, j] = 0
                break
    return contour_copy

def fill_contour(contour_copy):
    final = copy.deepcopy(contour_copy)
    count = 0
    start = 0
    for i in range(1, np.shape(contour_copy)[0] - 1):
        for j in range(1, np.shape(contour_copy)[1] - 1):
            if contour_copy[i, j] == 0 and contour_copy[i, j - 1] == 1 and contour_copy[i, j + 1] == 1:
                count += 1
            if contour_copy[i, j] == 0 and contour_copy[i, j - 1] == 1 and contour_copy[i, j + 1] == 0:
                start = 1
            if start == 1 and contour_copy[i, j] == 0 and contour_copy[i, j + 1] == 1:
                start = 0
                count += 1
        if count == 2:
            count2 = 0
            for j in range(1, np.shape(contour_copy)[1]):
                if contour_copy[i, j] == 0 and contour_copy[i, j - 1] == 1 and contour_copy[i, j + 1] == 1:
                    count2 += 1
                if contour_copy[i, j] == 0 and contour_copy[i, j - 1] == 1 and contour_copy[i, j + 1] == 0:
                    start = 1
                if start == 1 and contour_copy[i, j] == 0 and contour_copy[i, j + 1] == 1:
                    start = 0
                    count2 += 1
                if count2 == 1 and contour_copy[i, j] == 1:
                    final[i, j] = 0
                    k = i
                    while(contour_copy[k - 1, j] == 1):
                        final[k - 1, j] = 0
                        k -= 1
                    l = i
                    while(contour_copy[l + 1, j] == 1):
                        final[l + 1, j] = 0
                        l += 1
        count = 0
    return final

def build_solution(img, final):
    out = np.zeros(shape = np.shape(img))
    for i in range(vertical_min, vertical_max):
        for j in range(horizontal_min, horizontal_max):
            if final[i - vertical_min, j - horizontal_min] == 0:
                out[i, j] = 1
    return out

def show(img, name):
    img_copy = copy.deepcopy(img)
    img_copy = np.matrix(img_copy)
    with open(name, 'wb') as f:
        for line in img_copy:
            np.savetxt(f, line, fmt='%d')

scan_image = '107-HU.in'
segmentation_image = '107-seg.in'
delta = 65
weak_links_size = 3
max_noise_size = 500
pad_down_size = 16
pad_up_size = 16
pad_left_size = 16
pad_right_size = 16
max_path_size = 160

#scan_image = '89-HU.in'
#segmentation_image = '89-seg.in'
#delta = 62
#weak_links_size = 3
#max_noise_size = 150
#pad_down_size = 50
#pad_up_size = 50
#pad_left_size = 50
#pad_right_size = 50
#max_path_size = 80

#scan_image = '121-HU.in'
#segmentation_image = '121-seg.in'
#delta = 88
#weak_links_size = 2
#max_noise_size = 350
#pad_down_size = 100
#pad_up_size = 100
#pad_left_size = 100
#pad_right_size = 40
#max_path_size = 320

#scan_image = '187-HU.in'
#segmentation_image = '187-seg.in'
#delta = 22
#weak_links_size = 10
#max_noise_size = 500
#pad_down_size = 20
#pad_up_size = 20
#pad_left_size = 20
#pad_right_size = 20
#max_path_size = 80

img, seg = read_data(scan_image, segmentation_image)

vertical_min, vertical_max, horizontal_min, horizontal_max = get_interest_zone_boundaries(seg)

seg_edges = get_segmentation_edges(seg, vertical_min, vertical_max, horizontal_min, horizontal_max)
img_edges = get_noisy_image_edges(img, vertical_min, vertical_max, horizontal_min, horizontal_max, delta)
img_edges = delete_weak_links(img_edges, weak_links_size)

img_denoised = get_denoised_image(img_edges, max_noise_size)
img_denoised = pad_down(img_denoised, pad_down_size)
img_denoised = pad_up(img_denoised, pad_up_size)
img_denoised = pad_left(img_denoised, pad_left_size)
img_denoised = pad_right(img_denoised, pad_right_size)
img_denoised = approximate_corners(img_denoised)

contour = get_image_contour(img_denoised)
inside = get_inside_sections(seg_edges)
outside = get_outside_sections(seg_edges)
inside, outside = eliminate_single_pixels(inside, outside)

d = get_start_end_points_of_outside_sections(outside)
corrected_contour, contour = correct_outside_sections(contour, d)
corrected_contour = correct_inside_sections(inside, d, contour, corrected_contour, max_path_size)

shape = fill_contour(corrected_contour)
out = build_solution(img, shape)
show(out, 'optim.out')