img, seg = read_data(scan_image, segmentation_image)
#aceasta functie citeste datele de intrare, scan_image fiind imaginea fara
nicio sectionare si segmentation_image fiind imaginea cu sectionarea facuta
de medic si le transforma in matricile img respectiv seg

vertical_min, vertical_max, horizontal_min, horizontal_max = get_interest_zone_boundaries(seg)
#aceasta functie identifica pozitionarea segmentarii relativ la intreaga 
imagine si returneaza aceste coordonate cu o toleranta de 5% din pixelii
imaginii. Ideea este sa facem intreaga prelucrare doar pe zona de interes si
nu pe intreaga imagine economisind astfel efort computational, toleranta de 5%
fiind considerata pentru situatia in care segmentarea facuta de medic intra 
si in interiorul organului caz in care dorim sa vedem si ceea ce se afla
in proximitate cu segmentarea facuta de medic.

seg_edges = get_segmentation_edges(seg, vertical_min, vertical_max, horizontal_min, horizontal_max)
#aceasta functie returneaza marginile segementarii facute de medic, altfel 
spus conturul. Algoritmul foloseste o copie a imaginii cu segmentarea facuta
de medic si consta in ideea de a parcurge imaginea originala cu segmentarea 
facuta de medic in zona de interes si a itera peste aceasta zona cu o fereastra
3x3. Daca toate valorile binare din aceasta fereastra corespund valorii
binare a sectionarii facute de medic atunci elementul din mijlocul acestei 
ferestre se afla in interiorul sectionarii facute de medic si schimbam in 
imaginea copie valoarea din mijloc cu valoarea binara corespunzatoare 
exteriorului sectionarii facute de medic. Astfel in imaginea copie care 
reprezinta de altfel si rezultatul acestei functii se obtine conturul.  

img_edges = get_noisy_image_edges(img, vertical_min, vertical_max, horizontal_min, horizontal_max, delta)
img_edges = delete_weak_links(img_edges, weak_links_size)
img_denoised = get_denoised_image(img_edges, max_noise_size)
#Incercare1:
	Prima metoda de abordare a problemei a fost eliminarea 
zgomotului prin parcurgerea imaginii cu o fereastra 3x3 si inlocuind valoarea 
elementului din mijloc cu media elementelor ferestrei. De asemenea am incercat 
si parcurgerea cu ferestre mai mari 5x5 sau 7x7 sau iterarea de mai multe ori a
acestui algoritm. In cele din urma nu am fost multumit de rezultate deoarece 
aceasta metoda de eliminare a zgomotului elimina zgomotul in zone bune precum 
in interiorul organului dar de asemenea algoritmul facea medierea la granita 
organului ceea ce facea ca odata cu disparitia zgomotului din interior, organul 
sa fie din ce in ce mai greu de definit in acelasi timp costul computational 
fiind foarte mare.

#Incercare2:
	Cea de-a doua incercare a fost sa creez un algoritm care sa elimine
zgomotul in mod inteligent care sa stie atunci cand se afla la granita organului
sau in interiorul acestuia. Mai intai se parcurge imaginea cu o fereastra 3x3
si se calculeaza cel mai mic si cel mai mare element din fereastra iar diferenta 
dintre maxim si minim reprezinta o valoare numita delta. Daca acest delta
este mai mic de un anumit prag ales de noi inseamna ca avem de-a face cu o zona
din interiorul unui organ daca delta este mai mare decat pragul ales atunci acea
portiune trebuie marcata drept o zona de granita a organului. Aceasta marcare 
este binara. Acest algoritm va identifica atat granita organului cat si zgomotul
cu aceeasi valoare binara deoarece ambele sunt descrise prin valori ale deltei
mai mari decat pragul. In continuare se parcurge aceasta imagine binara cu o 
fereastra de o dimensiune aleasa de noi drept marimea maxima a zgomotului. 
Daca interiorul organului este reprezentat cu 0 si marginea cu 1, iar in 
matricea de demnsiunea maxima a zgomotului pe prima linie, ultima linie, prima 
coloana si ultima coloana avem doar valori de 0 iar in interiorul matricii avem 
valori de 1 putem considera ca acele valori de 1 daca pot fi cuprinse de 
matricea noastra reprezinta zgomot si nu o margine. O margine nu poate fi
cuprinsa cu aceasta matrice doarece matricea este mica comparativ cu marimea
organului. Acest algoritm in aceasta forma nu este eficient deoarce fereastra
fiind fixa poate avea probleme in a identifica bucatile mici de zgomot
deoarece se pot afla in apropierea unor bucati de zgomot mai mari. Astfel se 
impune parcurgerea acestui algoritm de mai multe ori incepand cu valori mici
ale ferestrei pana la fereastra care reprezinta marimea maxima a zgomotului
estimata de noi. Problema acestui algoritm consta in efortul mare 
computational si incapacitatea de a elimina zgomotul care este aproape de
granita oragnului. Acest algoritm are nevoie ca zonele de zgomot sa fie 
departate intre ele pentru a le putea cuprinde iar acest lucru nu se intampla 
cu zgomotul din apropiera granitei.

#Incercare3:
	Cea de-a treia incercare obtine ca mai devreme aceeasi imagine binara
in functie de pragul ales de noi. De aceasta data in schimb se schimba 
paradigma. Daca mai devreme am irosit mult efort computational iterand peste
imagine cu diverse ferestre si intreband la fiecare pas "Avem de a face cu
zgomot?" de cele mai multe ori intreband fara sa avem de a face cu zgomot, 
de aceasta data nu mai intrebam ci vom determina singuri ecnomisind astfel 
mult efort computational. Acest algoritm itereaza peste imagine si se opreste
doar atunci cand gaseste o valoare corespunzatoare granitei organului sau 
zgomotului, in loc sa intrebe la fiecare pas ca mai devreme. Din acest punct
se merge in sus cat timp continuam sa avem valori corespunzatoare marginii/
zgomtoului, de asemenea in jos si la dreapta si se retin aceste valori 
drept latime si intaltime. Vom construi o matrice care sa aiba dimensiunea 
(latime + 2)x(inaltime + 2), acel +2 fiind deoarece incercam sa bordam
ceea ce presupunem ca poate fi zgomot. Parcurgem granita matricei obtinuta
iar daca undeva pe granita observam o valoare corespunzatoare marginii/
zgomotului din acel punct mergem din nou in sus, jos, stanga si dreapta cat 
timp avem valoari corespunzatoare marginii/zgomotului si bordam din nou
latimea si inaltimea obtinute. Astfel in acest moment avem doua ferestre 
pe care le imbinam intr-una singura care sa cuprinda zonele identificate de cele
doua matrici determinate anterior. Se continua recursiv cu acest algoritm iar 
daca atingem o marime de prag ne oprim si nu mai continuam doarece daca 
continuam la nesfarsit ne vom opri in cele din urma in anumite situatii atunci
cand cuprindem intreaga imagine. Pentru a optimiza acest algoritm atunci
cand atingem pragul si ne decidem ca acele valori fac parte din margine
si nu sunt zgomot vom marca valorile pe care le-am cuprins in acea matrice
pentru a nu incerca sa cautam daca avem de-a face cu zgomot sau o margine 
din nou atunci cand iteram peste imagine. Din punct de vedere computational
acest algoritm este cel mai bun de pana acum insa are o problema in ideea
pe care o implementeaza anume faptul ca incearca sa elimine zgomotul care
poate avea forme diverse aproximandu-l printr-o matrice, de aceea atunci
cand sunt bucati de zgomot apropiate unele de altele acest algoritm se extinde
in incercarea de a curpinde zgomotul fara sa-si dea seama ca in el exista 
deja mai multe bucati individuale de zgomot de forme diverse dar el se extinde
crezand ca are de-a face cu o singura bucata de zgomot.

#Incercare4:
	Acest algoritm este foarte asemanator cu cel de mai devreme, marea 
diferenta fiind ca de aceasta data in loc sa incercam sa aproximam zgomotul 
cu o matrice vom identifica zgomotul exact in forma pe care o are construind
un arbore. Daca mai devreme atunci cand parcurgeam imaginea binara obtinuta
gaseam o valoare corespunzatoare marginii/zgomotului incepeam sa construim
o matrice, de aceasta data incepem sa construim un arbore si vom considera o
valoare de prag drept marimea maxima a zgomotului pentru a opri arborele 
din a se extinde atunci cand ne aflam pe o margine a organului. Acest algoritm
este cel mai eficient din punct de vedere computational de pana acum si in 
acelasi timp reuseste sa elimine zgomotul cel mai eficient. Ca metoda de 
optimizare ne vom marca bucatile parcurse de arborele care s-a oprit deoarce
am constatat ca avem de a face cu o margine a organului si nu cu zgomot. 
Totodata se observa ca marginea obtinuta a organului, in sine contine zgomot
care nu poate fi eliminat de acest algoritm deoarece parcurgand conturul
nu poate determina care bucata din contur reprezinta zgomot si care nu.
Pentru a ne rezolva aceasta problema si in acelasi timp a ne folosi mai bine
de acest algoritm, inainte de a-l aplica, se observa ca bucatile de zgomot
sunt legate de contur printr-un singur pixel sau doi in general, atunci
aplicam functia denumita delete_weak_links care sa parcurga imaginea binara
zgomotoasa fie cu o fereastra 2x2 pentru a elimina legaturile de un pixel
sau 3x3 pentru a elimina legaturile de 2 pixeli, noi decidem, 
care creeaza o copie a imaginii binare zgomotoase iar pe acea copie pastreaza           
din imaginea orginiala doar acele zone care intr-o matrice 2x2 sau 3x3 au 
aceeasi valoare binara. Practic functia delete_weak_links face ca imaginea
sa fie mai patratoasa, eliminand legaturile de 1 pixel sau 2. Acest lucru duce
la pierderea in detalii ale conturului facandu-l mai patratos dar in acelasi 
timp acest compromis merita avand in vedere ca daca aplicam acest algoritm
dupa acest procedeu, algoritmul va elimina si zgomotul care era lipit de contur
si per ansamblu duce la un contur mai bun.    
	Functia get_noisy_image_edges realizeaza acea parte a algoritmului
de transformare a imaginii in una binara comparand delta din fereastra 3x3
cu care iteram peste imagine cu un prag ales de noi, considerand ca daca
delta este mai mica decat pragul avem de-a face cu o zona care reprezinta 
interiorul unui organ si atunci aplicam ideea de la algoritmul de detectie a
marginilor anume valoarea din mijlocul acestei ferestre 3x3 se considera a fi
in interiorul organului si se noteaza cu 1 altfel daca delta este mai mare 
decat pragul valoarea din mijlocul ferestrei 3x3 ramane 0. In program am dat
ca parametru acestei functii o variabila numita delta, aceasta variabila
reprezinta de fapt pragul din algoritmul descris mai sus.
	Functia delete_weak_links elimina legaturile slabe din imaginea 
zgomotoasa, paramterul weak_links_size reprezantand marimea maxima a legaturilor 
pe care dorim sa le eliminam.
	Functia get_denoised_image reprezinta algoritmul de eliminare a 
zgomutului descris mai sus unde paramterul max_noise_size reprezinta marimea
maxima a arboreului, echivalent cu marimea maxima a zgomotului pe care dorim
sa-l eliminam.

img_denoised = pad_down(img_denoised, pad_down_size)
img_denoised = pad_up(img_denoised, pad_up_size)
img_denoised = pad_left(img_denoised, pad_left_size)
img_denoised = pad_right(img_denoised, pad_right_size)
img_denoised = approximate_corners(img_denoised)
#Aceste functii ajuta atunci cand vom prelucra conturul segmentarii facute
de medic folosindu-ne de imaginea din care am eliminat zgomotul. Functiile
pad cnostau in faptul ca daca in imagine exista "scobituri" acestea vor fi
umplute. Exemplu de "scobituri":

000            0  0          0000            000
  0 - pad_left 0  0 - pad_up 0  0 - pad_down 0   - pad_right
000            0000          0  0            000 

Practic o "scobiutra" inseamna un interval marginit de unghiuri de 90 de grade.
Paramterii pad_down_zise, pad_up_size, pad_left_size, pad_right_size reprezinta
marimea maxima pe care trebuie sa o aiba o "scobiutra" pentru a fi umpluta.
Aceasta functie este utila deoarece dupa ce aplicam algoritmul de eliminare a
zgomotului de multe ori in afara organului vor ramane zone mari de zgomot si 
dese care nu au fost eliminate deoarece prezinta legaturi puternice cu zona de 
granita a organului sau legaturi puternice intre ele, atunci functiile pad vor
umple spatiile dintre aceste zone si per ansamblu in imagine va exista o 
delimitare mai buna intre interiorul organului si exteriorul acestuia.
	Functia approximate_corners va aproxima unghiurile de 90 de grade cu
o linie oblica, aceasta finctie considera si cazurile "scobitura" caz in care
va face o aproximare a ambelor unghiuri de 90 de grade cu o linie oblica, cele
doua linii oblice intalnindu-se in mijlocul scobiturii.

contour = get_image_contour(img_denoised)
#aceasta functie obtine conturul/contururile care se obtin in imaginea 
prelucrata din care am eliminat zgomotul. Folosim acelasi algoritm de detectie
a marginilor pe care l-am folosit si anterior.  

inside = get_inside_sections(seg_edges)
#aceasta functie determina bucatile din sectionarea facuta de medic care se afla
in interiorul organului, comparand sectionarea facuta de medic cu imaginea 
prelucrata din care am eliminat zgomotul.

outside = get_outside_sections(seg_edges)
#aceasta functie determina bucatile din sectionarea facuta de medic care se afla
in exteriorul organului, comparand sectionarea facuta de medic cu imaginea 
prelucrata din care am eliminat zgomotul.

inside, outside = eliminate_single_pixels(inside, outside)
#problema cu algoritmii folositi mai devreme de determinare a bucatilor din
sectionarea facuta de medic care se afla in interiorul respectiv exteriorul
organului este ca acestia prezinta drept surse de erori faptul ca atunci cand 
imaginea prelucrata are zone de contur care se extind pe diagonala, lucru
care se intampla prin unghiuri de 90 de grade foarte mici, se poate intampla
ca un singur pixel sa fie identificat ca apartine interiorului organului iar
restul exteriorului ceea ce conduce la lipsa unui pixel dintr-un unghi de 90 de 
grade format din 3 pixeli, adica raman 2 pixeli care reprezinta o diagonala acum
iar cel lipsa este adaugat in cealalta matrice. Problema e ca unghiul de 90
de grade transformat in diagonala nu reprezinta o discontinuitate in conturul
segmentarii facute de medic desi matricea opusa sugereaza acest lucru prin 
faptul ca are un pixel in acea zona. De aceea de cate ori avem un singur pixel
fie in matricea inside sau outside acesta reprinta o eroare si trebuie corectat, 
fiind pus in matricea opusa.

d = get_start_end_points_of_outside_sections(outside)
#aceasta functie se uita in matricea outside care contine zonele din segmentarea
facuta de medic care se afla in afara organului si creeaza un dictionar cu 
punctul din care aceste bucati incep si punctul in care aceste bucati se 
termina. Avem practic coordonatele de start si sfarsit ale tuturor bucatilor
din segmentarea facuta de medic care se afla in afara organului.

corrected_contour, contour = correct_outside_sections(contour, d)
#avand coordonatele de start si stop ale bucatilor din afara organului le 
corectam mergand de la punctul de start la punctul de stop pe conturul obtinut
de noi din imaginea prelucrata din care am eliminat zgomotul.

corrected_contour = correct_inside_sections(inside, d, contour, corrected_contour, max_path_size)
#avand bucatile segmentarii facute de medic care se afla in interiorul organului
si dictionarul care contine coordonatele de start si de stop ale bucatilor
segmentarii facute de medic din afara organului putem determina un nou dictionar
care contine punctele de start si de stop de pe contur pentru bucatile din 
interior. Problema care apare acum si nu aparea mai devreme este faptul ca 
exista posibilitatea existentei unor "gauri" in conturul care delimiteaza 
organul, iar algoritmul doreste sa ajunga din punctul de start in punctul de
stop mergand pe contur insa deorece intre start si stop exista o "gaura" acesta
nu va putea ajunge niciodata la destinatie. De aceea functia are parametrul 
max_path_size care reprezinta cat de mult vom merge pe contur in incercarea
de a ajunge de la start la stop pana cand decidem ca am mers prea mult punct in
care ne oprim si stabilim ca avem de-a face cu o "gaura" in contur. Daca avem
de-a face cu o "gaura" in contur atunci pastram segmentarea facuta de medic pe
acea portiune de start-stop. 

shape = fill_contour(corrected_contour)
#corrected_contour reprezinta in acest moment conturul corectat al organului
pe care medicul a incercat sa il segmenteze. Acum acest contur trebuie umplut.
Algoritmul folosit pentru a umple conturul consta in a itera peste imaginea care 
contine acest contur si a trece rezultatul prelucrarii pe o copie a acestei
imagini. Atunci cand se itereaza peste imaginea orginala care contine conturul
ne oprim pe fiecare linie in primul punct care contine valoarea  binara 
specifica conturului si vom umple pe acea linie in imaginea copie pana cand 
intalnim din nou valoarea binara specifica conturlui in imaginea orginala.
In timp ce se face aceasta umplere pe linie ne oprim in fiecare punct al acestui
proces si mergem sus si jos pana cand intalnim valoarea binara specifica 
conturlui pe imaginea originala si facem umplerea pe imaginea copie. Acest
algoritm reuseste sa umple forme neregulate de un anumit grad. Cu cat marim
complexitatea acestui algoritm cu atat va fi capabil sa umple forme mai 
neregulate, spre exemplu atunci cand se merge sus-jos daca la fiecare pas al
acestui proces am umple stanga-dreapta si asa mai departe insa complexitatea
creste cu o putere in plus la adaugarea unui nou astfel de pas. Presupunand ca 
organele nu prezinta forme foarte neregulate algoritmul folosit a reusit cu
succes sa umple formele din cele 4 exemple.     

out = build_solution(img, shape)
#aceasta functie reasaza figura umpluta in contextul imaginii de 512x512 
deoarece noi am efectuat toate aceste prelucrari doar pe zona de interes.

show(out, 'optim.out')
#aceasta functie scrie matricea rezultat in fisierul "optim.out".

In concluzie consider ca scopul problemei a fost atins. S-a luat o problema
care in limbaj natural suna precum "corectati conturul unui medic care incearca
sa sectioneze un organ bazat pe o imagine care reprezinta o scanare a unei 
sectiuni din corpul uman" si aceasta problema a fost transpusa intr-un limbaj
care poate fi inteles de catre calculator printr-o functie care depinde de 
anumiti parametrii. Faptul ca am putut paramteriza problema din limbaj natural
este una  dintre etape, in continuare aceasta functie putand fi optimizata
de catre un sistem cu inteligenta artificiala care sa invete cum sa optimizeze
parametrii acestei functii si a ajunge la rezultatul dorit.

Parametrii principali, care au fost explicati mai sus, ai programului si 
valorile lor pentru cele 4 exemple sunt:
 
scan_image = '107-HU.in'
segmentation_image = '107-seg.in'
delta = 65
weak_links_size = 3
max_noise_size = 500
pad_down_size = 16
pad_up_size = 16
pad_left_size = 16
pad_right_size = 16
max_path_size = 160

scan_image = '89-HU.in'
segmentation_image = '89-seg.in'
delta = 62
weak_links_size = 3
max_noise_size = 150
pad_down_size = 50
pad_up_size = 50
pad_left_size = 50
pad_right_size = 50
max_path_size = 80

scan_image = '121-HU.in'
segmentation_image = '121-seg.in'
delta = 88
weak_links_size = 2
max_noise_size = 350
pad_down_size = 100
pad_up_size = 100
pad_left_size = 100
pad_right_size = 40
max_path_size = 320

scan_image = '187-HU.in'
segmentation_image = '187-seg.in'
delta = 22
weak_links_size = 10
max_noise_size = 500
pad_down_size = 20
pad_up_size = 20
pad_left_size = 20
pad_right_size = 20
max_path_size = 80

Acesti parametrii au fost trecuti si in program.
Descrisi sumar, acesti parametrii au urmatoarele efecte:

delta: o valoare mica va face ca imaginea sa fie mai zgomotoasa si o valoare
mai mare va face ca imaginea sa aiba mai putin zgomot.

weak_links_size: cu cat valoarea este mai mare cu atat imaginea isi pierde din
detalii si devine mai patratoasa.

pad_down_size: marimea "scobiturilor" orientate in jos care vor fi umplute

pad_up_size: marimea "scobiturilor" orientate in sus care vor fi umplute

pad_left_size: marimea "scobiturilor" orientate la stanga care vor fi umplute

pad_right_size: marimea "scobiturilor" orientate la dreapta care vor fi umplute

max_path_size: marimea maxima a traseului de pe contur care merge de la start 
la stop pentru a corecta segmentarile facute in interiorul organului de catre 
medic care daca este depasita se considera a fi "gaura" in contur. 

Observatie: Daca parametrii nu sunt alesi corect se vor obtine erori diverse
de la depasirea adincimii maxime de recursivitate la probleme de tip 
cheie-valoare atunci cand se construieste dictionarul sectiunilor din interiorul
sau exteriorul organului sau probleme de index atunci cand se itereaza peste
imagini.